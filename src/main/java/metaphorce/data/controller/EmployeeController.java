package metaphorce.data.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpServerErrorException.InternalServerError;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.NotFoundException;
import metaphorce.data.application.EmployeeService;
import metaphorce.data.domain.model.EmployeeDetails;
import metaphorce.data.domain.model.EmployeeV1;
import metaphorce.data.exception.BadRequestException;
import metaphorce.data.exception.ConflictException;

@Controller
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@ApiOperation(value = "Retrieves active employees.", responseContainer = "List", response = EmployeeDetails.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retrieves the active employees.", responseContainer = "application/json", response = EmployeeDetails.class),
			@ApiResponse(code = 400, message = "Bad Request response indicating that the server cannot process the request due to malformed request syntax.", responseContainer = "List", response = BadRequestException.class),
			@ApiResponse(code = 404, message = "Not Found response indicating that no active accounts was found", responseContainer = "List", response = NotFoundException.class),
			@ApiResponse(code = 500, message = "Internal Server Error response indicating that the server encountered an unexpected condition that prevented it from fulfilling the request.", responseContainer = "List", response = InternalServerError.class) })
	@GetMapping(value = "/v1/activeEmployees", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<EmployeeDetails>> getActiveEmployees() {
		return ResponseEntity.of(employeeService.getActiveEmployees());
	}

	@ApiOperation(value = "Create a new employee.")
	@ResponseStatus(value = HttpStatus.CREATED)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Indicates that the employee has been created"),
			@ApiResponse(code = 400, message = "Bad Request response indicating that the server cannot process the request due to malformed request syntax.", responseContainer = "List", response = BadRequestException.class),
			@ApiResponse(code = 409, message = "Conflict: the employee to try to create is already exist", responseContainer = "List", response = ConflictException.class),
			@ApiResponse(code = 500, message = "Internal Server Error response indicating that the server encountered an unexpected condition that prevented it from fulfilling the request.", responseContainer = "List", response = InternalServerError.class) })
	@PostMapping(value = "/v1/employee", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<EmployeeV1>> createEmployee(
			@ApiParam(value = "The request to process a contract", required = true) @Valid @RequestBody EmployeeV1 employee) {
		employeeService.saveEmployee(employee);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@ApiOperation(value = "Update existing Employee", notes = "This operation updates an existing employee.")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	@ApiResponses(value = {
			@ApiResponse(code = 204, message = "Indicates that an employee has been updated", responseContainer = "application/json"),
			@ApiResponse(code = 400, message = "Bad Request response indicating that the server cannot process the request due to malformed request syntax, invalid message framing, etc., and request must not be repeated without modification.", responseContainer = "List", response = BadRequestException.class),
			@ApiResponse(code = 404, message = "Not Found response indicating that the service may be down", responseContainer = "List", response = NotFoundException.class),
			@ApiResponse(code = 409, message = "Conflict: the employee to update does not exist", responseContainer = "List", response = ConflictException.class),
			@ApiResponse(code = 500, message = "Internal Server Error response indicating that the server encountered an unexpected condition that prevented it from fulfilling the request.", responseContainer = "List", response = InternalServerError.class) })
	@PutMapping(value = "/v1/employee/{employeeId}", produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Void> updateEmployee(
			@ApiParam(value = "The request to process a employee", required = true) @Valid @RequestBody EmployeeV1 employee,
			@ApiParam(value = "The unique identifier for the employee.", required = true) @PathVariable(required = true) String employeeId) {

		employeeService.updateAccount(employee, employeeId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}