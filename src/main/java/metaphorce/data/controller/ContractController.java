package metaphorce.data.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpServerErrorException.InternalServerError;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.NotFoundException;
import metaphorce.data.application.ContractService;
import metaphorce.data.domain.model.ContractV1;
import metaphorce.data.domain.model.EmployeeV1;
import metaphorce.data.exception.BadRequestException;

@Controller
public class ContractController {

  @Autowired
  private ContractService contractService;
  
  @ApiOperation(value = "Create a new contract.")
  @ResponseStatus(value = HttpStatus.CREATED)
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Indicates that the contract has been created"),
      @ApiResponse(code = 400,
              message = "Bad Request response indicating that the server cannot process the request due to malformed request syntax.",
              responseContainer = "List", response = BadRequestException.class),
      @ApiResponse(code = 500,
              message = "Internal Server Error response indicating that the server encountered an unexpected condition that prevented it from fulfilling the request.",
              responseContainer = "List", response = InternalServerError.class)})
  @PostMapping(value = "/v1/{employeeId}/contract", produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<List<EmployeeV1>> asignedContract(
		  @ApiParam(value = "The request to process a contract", required = true) @Valid @RequestBody ContractV1 contract,
		  @ApiParam(value = "The unique identifier for the employee.", required = true) @PathVariable(required = true) String employeeId) {
	  contractService.saveContract(contract, employeeId);
	  return new ResponseEntity<>(HttpStatus.CREATED);
  }

  
  @ApiOperation(value = "Delete contract.")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	@ApiResponses(value = {
	  @ApiResponse(code = 204, message = "A 204 No content indicating that the contract was deleted."),
	  @ApiResponse(code = 400, message = "Bad Request response indicating that the server cannot process the request due to malformed request syntax, invalid message framing, etc., and request must not be repeated without modification.", responseContainer = "List", response = BadRequestException.class),
	  @ApiResponse(code = 404, message = "Not Found response indicating that the server did not found the target resource or is not willing to disclose its existence, either temporary or permanent.", responseContainer = "List", response = NotFoundException.class),
	  @ApiResponse(code = 500, message = "Internal Server Error response indicating that the server encountered an unexpected condition that prevented it from fulfilling the request.", responseContainer = "List", response = InternalServerError.class)})
	@DeleteMapping(value = "/v1/{employeeId}/contract/{contractId}", produces = {"application/json"})
	public ResponseEntity<Void> deleteContract(
	  @ApiParam(value = "The unique identifier for a employee.", required = true) @PathVariable String employeeId,
	  @ApiParam(value = "The unique identifier for a contract.", required = true) @PathVariable String contractId) {
		contractService.cancelContract(employeeId, contractId);
	    return new ResponseEntity<>(HttpStatus.OK);
	}
  
}