package metaphorce.data.domain.model;

import java.time.LocalDate;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Contract information.")
public class ContractV1 {
    private String contractId;
    private String employeeId;
    private String contractTypeId;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private String salaryPerDay;
	public String getContractId() {
		return contractId;
	}
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getContractTypeId() {
		return contractTypeId;
	}
	public void setContractTypeId(String contractTypeId) {
		this.contractTypeId = contractTypeId;
	}
	public LocalDate getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(LocalDate dateFrom) {
		this.dateFrom = dateFrom;
	}
	public LocalDate getDateTo() {
		return dateTo;
	}
	public void setDateTo(LocalDate dateTo) {
		this.dateTo = dateTo;
	}
	public String getSalaryPerDay() {
		return salaryPerDay;
	}
	public void setSalaryPerDay(String salaryPerDay) {
		this.salaryPerDay = salaryPerDay;
	}
	
	@Override
	public String toString() {
		return "ContractV1 [contractId=" + contractId + ", employeeId=" + employeeId + ", contractTypeId="
				+ contractTypeId + ", dateFrom=" + dateFrom + ", dateTo=" + dateTo + ", salaryPerDay=" + salaryPerDay
				+ "]";
	}  
}
