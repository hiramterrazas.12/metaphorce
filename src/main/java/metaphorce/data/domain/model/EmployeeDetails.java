package metaphorce.data.domain.model;

import java.time.LocalDate;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Employee details.")
public class EmployeeDetails {
	private String fullName;
	private String taxIdNumber;
	private String email;
	private String contractTypeName;
	private LocalDate startContractDate;
	private LocalDate finalContractDate;
	private String salary;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getTaxIdNumber() {
		return taxIdNumber;
	}

	public void setTaxIdNumber(String taxIdNumber) {
		this.taxIdNumber = taxIdNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContractTypeName() {
		return contractTypeName;
	}

	public void setContractTypeName(String contractTypeName) {
		this.contractTypeName = contractTypeName;
	}

	public LocalDate getStartContractDate() {
		return startContractDate;
	}

	public void setStartContractDate(LocalDate startContractDate) {
		this.startContractDate = startContractDate;
	}

	public LocalDate getFinalContractDate() {
		return finalContractDate;
	}

	public void setFinalContractDate(LocalDate finalContractDate) {
		this.finalContractDate = finalContractDate;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

}
