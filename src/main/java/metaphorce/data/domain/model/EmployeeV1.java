package metaphorce.data.domain.model;

import java.time.LocalDate;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Employee information.")
public class EmployeeV1 {
	private String taxIdNumber;
	private String name;
	private String lastName;
	private LocalDate brithDay;
	private String email;
	private String cellPhone;

	public String getTaxIdNumber() {
		return taxIdNumber;
	}

	public void setTaxIdNumber(String taxIdNumber) {
		this.taxIdNumber = taxIdNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getBrithDay() {
		return brithDay;
	}

	public void setBrithDay(LocalDate brithDay) {
		this.brithDay = brithDay;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

}
