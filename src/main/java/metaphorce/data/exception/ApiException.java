package metaphorce.data.exception;
public class ApiException extends RuntimeException {
	private static final long serialVersionUID = 5715489724247704590L;

	private int code;

	public ApiException(int code, String title) {
		super(title);
		this.setCode(code);
	}

	public ApiException(int code, String title, Throwable cause) {
		super(title, cause);
		this.setCode(code);
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
}