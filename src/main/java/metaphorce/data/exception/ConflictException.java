package metaphorce.data.exception;
public class ConflictException extends ApiException {
	public ConflictException(int code, String title, Throwable cause) {
		super(code, title, cause);
	}

	public ConflictException(int code, String title) {
		super(code, title);
	}
}
