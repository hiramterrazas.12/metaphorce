package metaphorce.data.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

public class BadRequestException extends HttpClientErrorException {

	public BadRequestException(String title) {
		super(HttpStatus.BAD_REQUEST, title);
	}


}