package metaphorce.data.exception;
public class NotFoundException extends ApiException {
	public NotFoundException(int code, String title, Throwable cause) {
		super(code, title, cause);
	}

	public NotFoundException(int code, String title) {
		super(code, title);
	}
}