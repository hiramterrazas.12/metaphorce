package metaphorce.data.dao.entity.employee;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import metaphorce.data.dao.entity.contract.Contract;
import metaphorce.data.dao.entity.share.MetaData;
import metaphorce.data.domain.model.EmployeeDetails;
import metaphorce.data.domain.model.EmployeeV1;

@Entity
@Table(name = "Employee")
public class Employee extends MetaData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EmployeeId")
	private String employeeId;

	@Column(name = "TaxIdNumber")
	private String taxIdNumber;

	@Column(name = "Name")
	private String name;

	@Column(name = "LastName")
	private String lastName;

	@Column(name = "BrithDay")
	private LocalDate brithDay;

	@Column(name = "Email")
	private String email;

	@Column(name = "CellPhone")
	private String cellPhone;

	public String getTaxIdNumber() {
		return taxIdNumber;
	}
	
	public String getEmployeeId() {
		return employeeId;
	}

	public Employee of(EmployeeV1 request) {
		Employee entity = new Employee();
		entity.taxIdNumber = request.getTaxIdNumber();
		entity.name = request.getName();
		entity.lastName = request.getLastName();
		entity.brithDay = request.getBrithDay();
		entity.email = request.getEmail();
		entity.cellPhone = request.getCellPhone();
		entity.created();
		return entity;
	}

	public Employee update(Employee other) {
		this.taxIdNumber = other.taxIdNumber;
		this.name = other.name;
		this.lastName = other.lastName;
		this.brithDay = other.brithDay;
		this.email = other.email;
		this.cellPhone = other.cellPhone;
		return this;
	}
	
	public EmployeeDetails response(Employee employee) {
		EmployeeDetails response = new EmployeeDetails();
		response.setFullName(employee.name + " " + employee.lastName);
		response.setTaxIdNumber(taxIdNumber);
		response.setEmail(email);
		response.setContractTypeName(null);
		response.setStartContractDate(null);
		response.setFinalContractDate(null);
		response.setSalary(null);
		return response;
	}
	
	public EmployeeDetails response(Employee employee, Contract contract) {
		EmployeeDetails response = new EmployeeDetails();
		response.setFullName(employee.name + " " + employee.lastName);
		response.setTaxIdNumber(taxIdNumber);
		response.setEmail(email);
		response.setContractTypeName(contract.getContractTypeId());
		response.setStartContractDate(contract.getDateFrom());
		response.setFinalContractDate(contract.getDateTo());
		response.setSalary(contract.getSalaryPerDay());
		return response;
	}
}