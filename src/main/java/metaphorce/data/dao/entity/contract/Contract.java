package metaphorce.data.dao.entity.contract;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import metaphorce.data.dao.entity.share.MetaData;
import metaphorce.data.domain.model.ContractV1;

@Entity
@Table(name = "Contract")
public class Contract extends MetaData {

    @Id
    @Column(name = "ContractId")
    private String contractId;

    @Column(name = "EmployeeId")
    private String employeeId;

    @Column(name = "ContractTypeId")
    private String contractTypeId;

    @Column(name = "DateFrom")
    private LocalDate dateFrom;

    @Column(name = "DateTo")
    private LocalDate dateTo;

    @Column(name = "SalaryPerDay")
    private String salaryPerDay;    
    
    public String getEmployeeId() {
		return employeeId;
	}

	public Contract disableContract(Contract exitContract) {
    	exitContract.dateTo = LocalDate.now();
    	exitContract.disable();
    	return exitContract;
    }
	
	public boolean isActive() {
		return this.isActive();
	}
    
    public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getContractTypeId() {
		return contractTypeId;
	}

	public void setContractTypeId(String contractTypeId) {
		this.contractTypeId = contractTypeId;
	}

	public LocalDate getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(LocalDate dateFrom) {
		this.dateFrom = dateFrom;
	}

	public LocalDate getDateTo() {
		return dateTo;
	}

	public void setDateTo(LocalDate dateTo) {
		this.dateTo = dateTo;
	}

	public String getSalaryPerDay() {
		return salaryPerDay;
	}

	public void setSalaryPerDay(String salaryPerDay) {
		this.salaryPerDay = salaryPerDay;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public Contract of(ContractV1 request){
    	Contract entity = new Contract();
    	entity.contractId = request.getContractId();
    	entity.employeeId = request.getEmployeeId();
    	entity.contractTypeId = request.getContractTypeId();
    	entity.dateFrom = request.getDateFrom();
    	entity.dateTo = request.getDateTo();
    	entity.salaryPerDay = request.getSalaryPerDay();
        entity.created();
        return entity;
    } 
}