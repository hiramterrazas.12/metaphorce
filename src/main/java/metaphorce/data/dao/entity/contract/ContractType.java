package metaphorce.data.dao.entity.contract;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import metaphorce.data.dao.entity.share.MetaData;

@Entity
@Table(name = "ContractType")
public class ContractType extends MetaData {

    @Id
    @Column(name = "ContractTypeId")
    private String contractTypeId;

    @Column(name = "Name")
    private String name;

    @Column(name = "Description")
    private String description;
  
}