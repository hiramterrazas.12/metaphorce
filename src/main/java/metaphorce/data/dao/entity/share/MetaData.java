package metaphorce.data.dao.entity.share;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class MetaData {

	@Column(name = "isActive")
	private boolean isActive;

	@Column(name = "DateCreated")
	private Instant dateCreate;

	protected void created() {
		isActive = true;
		dateCreate = Instant.now();
	}
	
	protected void disable() {
		isActive = false;
	}
}
