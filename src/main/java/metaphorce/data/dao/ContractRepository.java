package metaphorce.data.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import metaphorce.data.dao.entity.contract.Contract;

public interface ContractRepository extends CrudRepository<Contract, String> {
	public Optional<Contract> findByEmployeeId(String employeeId);
}
