package metaphorce.data.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import metaphorce.data.dao.entity.employee.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, String> {
	public Optional<List<Employee>> findByIsActive(boolean isActive);
	
	public Optional<Employee> existsByTaxIdNumber(String taxIdNumber);
}
