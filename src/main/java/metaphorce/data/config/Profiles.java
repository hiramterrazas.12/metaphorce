package metaphorce.data.config;

public class Profiles {
    public static final String LOCAL = "local";
    public static final String UNIT_TEST = "test";
    
    private Profiles() {}
}
