package metaphorce.data.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile({Profiles.LOCAL})
public class ApplicationConfig {
    
}
