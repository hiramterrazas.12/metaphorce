package metaphorce.data.config;

import az.it.swagger.SpringfoxConfig;
import java.util.Arrays;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
@Import(SpringfoxConfig.class)
public class SwaggerConfig {

    @Autowired
    private Docket docket;

    @PostConstruct
    public void enhanceDocket(){
        docket.globalOperationParameters(Arrays.asList(new ParameterBuilder()
            .name("Request-Id")
            .description("Unique Request ID for logging/tracing purposes.")
            .modelRef(new ModelRef("string"))
            .parameterType("header")
            .required(false)
            .build()));

    }

}
