package metaphorce.data.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@ConfigurationProperties(prefix = "spring.metaphorce-datasource")
@EnableJpaRepositories(
        basePackages = {"metaphorce.data.dao"},
        entityManagerFactoryRef = "metaphorceEntityManagerFactory",
        transactionManagerRef = "metaphorceManager")
public class DbConfig {

  @Value("${spring.metaphorce-datasource.driver-class-name}")
  String driverClassName;

  @Value("${spring.metaphorce-datasource.url}")
  String url;

  @Value("${spring.metaphorce-datasource.username}")
  String username;

  @Value("${spring.metaphorce-datasource.password}")
  String password;

  @Value("${spring.metaphorce-datasource.hikari.maximum-pool-size}")
  int maximumPoolSize;

  @Value("${spring.metaphorce-datasource.hikari.minimum-idle}")
  int minimumIdle;

  @Value("${spring.metaphorce-datasource.hikari.connection-timeout}")
  int connectionTimeout;

  @Primary
  @Bean("metaphorceDataSource")
  public DataSource dataSource(){
    //connection data ↓↓↓
    HikariConfig hikariConfig = new HikariConfig();
    hikariConfig.setDriverClassName(driverClassName);
    hikariConfig.setJdbcUrl(url);
    hikariConfig.setUsername(username);
    hikariConfig.setPassword(password);
    // hikari data ↓↓↓
    hikariConfig.setMaximumPoolSize(maximumPoolSize);
    hikariConfig.setMinimumIdle(minimumIdle);
    hikariConfig.setConnectionTimeout(connectionTimeout);
    HikariDataSource dataSource = new HikariDataSource(hikariConfig);
    return dataSource;
  }

  @Primary
  @Bean("metaphorceEntityManagerFactory")
  @ConfigurationProperties(prefix = "spring.metaphorce-datasource")
  public LocalContainerEntityManagerFactoryBean loyaltyManagerFactory(
          @Qualifier("metaphorceDataSource") DataSource dataSource,
          @Value("${spring.jpa.database-platform}") String dialect,
          @Value("${hibernate.show_sql:false}") boolean showSql,
          @Value("${hibernate.format_sql:true}") boolean formatSql) {

    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();

    LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
    factory.setJpaVendorAdapter(vendorAdapter);
    factory.setPackagesToScan("metaphorce.data.dao");
    factory.setDataSource(dataSource);

    Properties jpaProperties = new Properties();
    jpaProperties.put("hibernate.dialect", dialect);

    // This needs to be set in order to store dates/times as UTC. See the following article:
    // https://vladmihalcea.com/how-to-store-date-time-and-timestamps-in-utc-time-zone-with-jdbc-and-hibernate/
    jpaProperties.put("hibernate.jdbc.time_zone", "UTC");
    jpaProperties.put("hibernate.show_sql", showSql);
    jpaProperties.put("hibernate.format_sql", formatSql);

    factory.setJpaProperties(jpaProperties);

    return factory;
  }

  @Primary
  @Bean("metaphorceManager")
  public PlatformTransactionManager xrefTransactionManager(
          @Qualifier("metaphorceEntityManagerFactory") LocalContainerEntityManagerFactoryBean entityManagerFactory) {
    JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
    return transactionManager;
  }
}