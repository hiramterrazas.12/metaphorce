package metaphorce.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties
@SpringBootApplication(scanBasePackages = {"metaphorce.data"})
public class Application {

  public static void main(String[] args) {
    new SpringApplication(Application.class).run(args);
  }
}
