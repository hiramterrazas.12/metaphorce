package metaphorce.data.application;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import metaphorce.data.dao.ContractRepository;
import metaphorce.data.dao.EmployeeRepository;
import metaphorce.data.dao.entity.contract.Contract;
import metaphorce.data.dao.entity.employee.Employee;
import metaphorce.data.domain.model.EmployeeDetails;
import metaphorce.data.domain.model.EmployeeV1;
import metaphorce.data.exception.BadRequestException;
import metaphorce.data.exception.ConflictException;
import metaphorce.data.exception.NotFoundException;

@Service
public class EmployeeService {
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private ContractRepository contractRepository;

	public Optional<List<EmployeeDetails>> getActiveEmployees() {
		List<EmployeeDetails> employees = new ArrayList<EmployeeDetails>();
		Optional<List<Employee>> activeEmployees = employeeRepository.findByIsActive(true);

		if (activeEmployees.isPresent()) {
			List<Employee> employeesActive = activeEmployees.get();
			
			for (Employee employee : employeesActive) {				
				Optional<Contract> contract = contractRepository.findById(employee.getEmployeeId());
				
				EmployeeDetails employeeDetails = null;
				if (contract.isPresent()) {
					if (!contract.get().isActive()) {
						employeeDetails = employee.response(employee);
					}
					employeeDetails = employee.response(employee, contract.get());
				} else {
					employeeDetails = employee.response(employee);
				}
				
				employees.add(employeeDetails);
			}
			
			return Optional.ofNullable(employees);
		} else {
			throw new NotFoundException(HttpStatus.NOT_FOUND.value(), "There is no employees active found in DB");
		}
	}

	public void saveEmployee(EmployeeV1 request) {
		if (validateRfc(request.getTaxIdNumber())) {
			throw new BadRequestException("The RFC is invalid");
		}

		if (employeeRepository.existsByTaxIdNumber(request.getTaxIdNumber()).isPresent()) {
			throw new ConflictException(HttpStatus.CONFLICT.value(), "This employee already exist");
		}

		Employee employee = new Employee().of(request);
		employeeRepository.save(employee);
	}

	public void updateAccount(EmployeeV1 request, String employeeId) {
		Employee existingEmployee = employeeRepository.findById(employeeId)
				.orElseThrow(() -> new ConflictException(HttpStatus.CONFLICT.value(),
						MessageFormat.format("The account {0} does not exists", employeeId)));
		
		if (existingEmployee.getTaxIdNumber() != request.getTaxIdNumber()) {
			if (validateRfc(request.getTaxIdNumber())) {
				throw new BadRequestException("The RFC is invalid");
			}
			
			if (employeeRepository.existsByTaxIdNumber(request.getTaxIdNumber()).isPresent()) {
				throw new ConflictException(HttpStatus.CONFLICT.value(), "This employee already exist");
			}
		}

		Employee requestEmployee = new Employee().of(request);
		existingEmployee.update(requestEmployee);
		employeeRepository.save(existingEmployee);

	}
	
	private boolean validateRfc(String rfc) {
		return rfc.toUpperCase().matches("[A-Z]{3,4}[0-9]{6}[A-Z0-9]{3}");
	}

}