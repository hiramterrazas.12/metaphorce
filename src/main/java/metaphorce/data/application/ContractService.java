package metaphorce.data.application;

import java.text.MessageFormat;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import metaphorce.data.dao.ContractRepository;
import metaphorce.data.dao.entity.contract.Contract;
import metaphorce.data.domain.model.ContractV1;
import metaphorce.data.exception.ConflictException;
import metaphorce.data.exception.NotFoundException;

@Service
public class ContractService {
	@Autowired
	private ContractRepository contractRepository;

	public void saveContract(ContractV1 contract, String employeeId) {
		Optional<Contract> exitContract = contractRepository.findByEmployeeId(employeeId);

		if (exitContract.isPresent()) {
			Contract contractUpdate = exitContract.get();
			contractUpdate = contractUpdate.disableContract(contractUpdate);
			contractRepository.save(contractUpdate);
		}

		Contract entityContract = new Contract().of(contract);
		contractRepository.save(entityContract);
	}

	public void cancelContract(String employeeId, String contractId) {
		Contract existingContract = contractRepository.findById(contractId)
				.orElseThrow(() -> new NotFoundException(HttpStatus.NOT_FOUND.value(),
						MessageFormat.format("The contract {0} does not exists", contractId)));

		if (existingContract.getEmployeeId() != employeeId) {
			throw new ConflictException(HttpStatus.CONFLICT.value(),
					"The contract {0} is assigned to different employee");
		}
		
		contractRepository.save(existingContract.disableContract(existingContract));
	}

}